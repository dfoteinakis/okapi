#!/bin/bash

cd "$(dirname "$0")"

JAVA=$(../../../check_java.sh)

if [ -z "$JAVA" ]; then
    exit 1
fi

# Get rid of process serial number from GUI launch
if [[ "$*" == -psn* ]]; then
    shift
fi

"$JAVA" -d64 -XstartOnFirstThread -Xdock:name="Rainbow" -jar ../../../lib/rainbow.jar "$@"
