package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.resource.CodeTypeBuilder;
import net.sf.okapi.filters.openxml.RunProperty.ToggleRunProperty;
import net.sf.okapi.filters.openxml.RunPropertyFactory.TogglePropertyName;

import static net.sf.okapi.common.resource.Code.TYPE_BOLD;
import static net.sf.okapi.common.resource.Code.TYPE_ITALIC;
import static net.sf.okapi.common.resource.Code.TYPE_LINK;
import static net.sf.okapi.common.resource.ExtendedCodeType.COLOR;
import static net.sf.okapi.common.resource.ExtendedCodeType.HIGHLIGHT;
import static net.sf.okapi.common.resource.ExtendedCodeType.SHADE;
import static net.sf.okapi.common.resource.ExtendedCodeType.SHADOW;
import static net.sf.okapi.common.resource.ExtendedCodeType.STRIKE_THROUGH;
import static net.sf.okapi.common.resource.ExtendedCodeType.SUBSCRIPT;
import static net.sf.okapi.common.resource.ExtendedCodeType.SUPERSCRIPT;
import static net.sf.okapi.common.resource.ExtendedCodeType.UNDERLINE;
import static net.sf.okapi.filters.openxml.RunContainer.Type.HYPERLINK;

/**
 * Provides a code type factory.
 */
class CodeTypeFactory {

    private static final String EMPHASIS_MARK_PROPERTY_NAME = "em";
    private static final String UNDERLINE_PROPERTY_NAME = "u";
    private static final String HIGHLIGHT_COLOR_PROPERTY_NAME = "highlight";
    private static final String SHADE_COLOR_PROPERTY_NAME = "shd";
    private static final String COLOR_PROPERTY_NAME = "color";
    private static final String VERTICAL_ALIGNMENT_PROPERTY_NAME = "vertAlign";

    private static final String VERTICAL_ALIGNMENT_SUPERSCRIPT_VALUE = "superscript";
    private static final String VERTICAL_ALIGNMENT_SUBSCRIPT_VALUE = "subscript";

    private static final String NONE_VALUE = "none";

    private static final boolean ADD_EXTENDED_CODE_TYPE_PREFIX = true;

    static String createCodeType(RunContainer runContainer) {

        CodeTypeBuilder codeTypeBuilder = new CodeTypeBuilder(ADD_EXTENDED_CODE_TYPE_PREFIX);

        if (HYPERLINK == runContainer.getType()) {
            codeTypeBuilder.addType(TYPE_LINK);
        } else {
            codeTypeBuilder.addType(runContainer.getType().getValue());
        }

        return codeTypeBuilder.build() + createCodeType(runContainer.getDefaultCombinedRunProperties(), !ADD_EXTENDED_CODE_TYPE_PREFIX);
    }

    static String createCodeType(RunProperties runProperties) {
        return createCodeType(runProperties, ADD_EXTENDED_CODE_TYPE_PREFIX);
    }

    private static String createCodeType(RunProperties runProperties, boolean addExtendedCodeTypePrefix) {

        CodeTypeBuilder codeTypeBuilder = new CodeTypeBuilder(addExtendedCodeTypePrefix);

        for (RunProperty runProperty : runProperties.getProperties()) {
            if (runProperty instanceof ToggleRunProperty) {
                handleToggleRunProperty(codeTypeBuilder, (ToggleRunProperty) runProperty);
                continue;
            }

            handleRunProperty(codeTypeBuilder, runProperty);
        }

        return codeTypeBuilder.build();
    }

    private static void handleToggleRunProperty(CodeTypeBuilder codeTypeBuilder, ToggleRunProperty toggleRunProperty) {
        if (!toggleRunProperty.getToggleValue()) {
            // get rid of "false" values
            return;
        }

        switch (TogglePropertyName.fromValue(toggleRunProperty.getName())) {
            case BOLD:
            case COMPLEX_SCRIPT_BOLD:
                codeTypeBuilder.addType(TYPE_BOLD);
                break;

            case ITALICS:
            case COMPLEX_SCRIPT_ITALICS:
                codeTypeBuilder.addType(TYPE_ITALIC);
                break;

            case STRIKE_THROUGH:
                codeTypeBuilder.addType(STRIKE_THROUGH.getValue());
                break;

            case SHADOW:
                codeTypeBuilder.addType(SHADOW.getValue());
                break;
        }
    }

    private static void handleRunProperty(CodeTypeBuilder codeTypeBuilder, RunProperty runProperty) {
        if (null == runProperty.getValue() || NONE_VALUE.equals(runProperty.getValue())) {
            // get rid of "null" and "none" values
            return;
        }

        if (EMPHASIS_MARK_PROPERTY_NAME.equals(runProperty.getName().getLocalPart())) {
            codeTypeBuilder.addType(TYPE_ITALIC);
            return;
        }

        if (UNDERLINE_PROPERTY_NAME.equals(runProperty.getName().getLocalPart())) {
            codeTypeBuilder.addType(UNDERLINE.getValue(), runProperty.getValue());
            return;
        }

        if (HIGHLIGHT_COLOR_PROPERTY_NAME.equals(runProperty.getName().getLocalPart())) {
            codeTypeBuilder.addType(HIGHLIGHT.getValue(), runProperty.getValue());
            return;
        }

        if (SHADE_COLOR_PROPERTY_NAME.equals(runProperty.getName().getLocalPart())) {
            codeTypeBuilder.addType(SHADE.getValue(), runProperty.getValue());
            return;
        }

        if (COLOR_PROPERTY_NAME.equals(runProperty.getName().getLocalPart())) {
            codeTypeBuilder.addType(COLOR.getValue(), runProperty.getValue());
            return;
        }

        if (VERTICAL_ALIGNMENT_PROPERTY_NAME.equals(runProperty.getName().getLocalPart())) {

            switch (runProperty.getValue()) {
                case VERTICAL_ALIGNMENT_SUPERSCRIPT_VALUE:
                    codeTypeBuilder.addType(SUPERSCRIPT.getValue());
                    return;
                case VERTICAL_ALIGNMENT_SUBSCRIPT_VALUE:
                    codeTypeBuilder.addType(SUBSCRIPT.getValue());
                    return;
                default:
                    // baseline is not supported
            }
        }
    }
}
