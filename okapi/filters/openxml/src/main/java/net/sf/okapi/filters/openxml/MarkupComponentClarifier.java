package net.sf.okapi.filters.openxml;

import static net.sf.okapi.filters.openxml.MarkupComponentClarifierStrategyFactory.createAlignmentMarkupComponentClarifierStrategy;
import static net.sf.okapi.filters.openxml.MarkupComponentClarifierStrategyFactory.createPresentationMarkupComponentClarifierStrategy;
import static net.sf.okapi.filters.openxml.MarkupComponentClarifierStrategyFactory.createSheetViewMarkupComponentClarifierStrategy;

/**
 * Provides a markup component clarifier.
 */
class MarkupComponentClarifier {

    private MarkupComponentClarifierStrategy strategy;

    MarkupComponentClarifier(MarkupComponentClarifierStrategy strategy) {
        this.strategy = strategy;
    }

    void clarify(MarkupComponent markupComponent) {
        strategy.clarifyMarkupComponent(markupComponent);
    }

    /**
     * Provides a presentation clarifier.
     */
    static class PresentationClarifier extends MarkupComponentClarifier {

        PresentationClarifier(CreationalParameters creationalParameters, ClarificationParameters clarificationParameters) {
            super(createPresentationMarkupComponentClarifierStrategy(creationalParameters, clarificationParameters));
        }
    }

    /**
     * Provides a sheet view clarifier.
     */
    static class SheetViewClarifier extends MarkupComponentClarifier {

        SheetViewClarifier(CreationalParameters creationalParameters, ClarificationParameters clarificationParameters) {
            super(createSheetViewMarkupComponentClarifierStrategy(creationalParameters, clarificationParameters));
        }
    }

    /**
     * Provides an alignment clarifier.
     */
    static class AlignmentClarifier extends MarkupComponentClarifier {

        AlignmentClarifier(CreationalParameters creationalParameters, ClarificationParameters clarificationParameters) {
            super(createAlignmentMarkupComponentClarifierStrategy(creationalParameters, clarificationParameters));
        }
    }
}
