package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.ISkeleton;

/**
 * Provides a markup skeleton.
 */
class MarkupSkeleton implements ISkeleton {
    private Markup markup;
    private IResource parent;

    public MarkupSkeleton(Markup markup) {
        this.markup = markup;
    }

    public Markup getMarkup() {
        return markup;
    }

    @Override
    public ISkeleton clone() {
        return new MarkupSkeleton(markup);
    }

    @Override
    public void setParent(IResource parent) {
        this.parent = parent;
    }

    @Override
    public IResource getParent() {
        return parent;
    }
}
