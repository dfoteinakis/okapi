package net.sf.okapi.filters.openxml;

class StyleExclusionChecker {

    static boolean isStyleExcluded(String style, ConditionalParameters params) {
        // If style is null or list of excluded styles is empty, nothing should be excluded:
        if (null == style || null == params.tsExcludeWordStyles || params.tsExcludeWordStyles.isEmpty()) {
            return false;
        }

        return params.tsExcludeWordStyles.contains(style);
    }
}