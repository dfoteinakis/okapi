/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.openxml;

import java.io.File;
import java.util.zip.ZipEntry;

import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.skeleton.ISkeletonWriter;

/**
 * Code to parse the _rels files present in Office OpenXML documents.
 */
public class OpenXMLSubDoc {

	private ZipEntry subDocEntry;
	private IFilterWriter subDocWriter;
	private ISkeletonWriter subSkelWriter;
	private File subDocTempFile;
	
	public OpenXMLSubDoc() {
	}
	public OpenXMLSubDoc(ZipEntry subDocEntry, IFilterWriter subDocWriter, 
			OpenXMLContentSkeletonWriter subSkelWriter, File subDocTempFile) {
		this.subDocEntry = subDocEntry; 
		this.subDocWriter = subDocWriter; 
		this.subSkelWriter = subSkelWriter;
		this.subDocTempFile = subDocTempFile;
	}
	
	public void setSubDocEntry(ZipEntry subDocEntry) {
		this.subDocEntry = subDocEntry; 
	}
	
	public ZipEntry getSubDocEntry() {
		return subDocEntry;
	}
	
	public void setSubDocWriter(IFilterWriter subDocWriter) {
		this.subDocWriter = subDocWriter; 
	}
	
	public IFilterWriter getSubDocWriter() {
		return subDocWriter;
	}
	
	public void setSubDocSkelWriter(ISkeletonWriter subSkelWriter) {
		this.subSkelWriter = subSkelWriter; 
	}
	
	public ISkeletonWriter getSubSkelWriter() {
		return subSkelWriter;
	}
	
	
	public void setSubTempFile(File subDocTempFile) {
		this.subDocTempFile = subDocTempFile; 
	}
	
	public File getSubDocTempFile() {
		return subDocTempFile;
	}	
}
