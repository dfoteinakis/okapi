package net.sf.okapi.filters.openxml;

import java.util.HashMap;
import java.util.Map;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.ISkeleton;

public class BlockSkeleton implements ISkeleton {
	private Block block;
	private IResource parent;
	private Map<Integer, XMLEvents> codeMap = new HashMap<>();

	public BlockSkeleton(Block block, Map<Integer, XMLEvents> codeMap) {
		this.block = block;
		this.codeMap = codeMap;
	}

	@Override
	public ISkeleton clone() {
		BlockSkeleton blockSkeleton =  new BlockSkeleton(block, codeMap);
		blockSkeleton.setParent(getParent());
		return  blockSkeleton;
	}

	public Block getBlock() {
		return block;
	}

	public Map<Integer, XMLEvents> getCodeMap() {
		return codeMap;
	}

	@Override
	public void setParent(IResource parent) {
		this.parent = parent;
	}

	@Override
	public IResource getParent() {
		return parent;
	}

}
