package net.sf.okapi.filters.openxml;

/**
 * Provides a property interface.
 */
public interface Property extends XMLEvents, Nameable {
}
