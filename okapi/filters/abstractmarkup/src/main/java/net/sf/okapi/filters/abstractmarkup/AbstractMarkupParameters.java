/*===========================================================================
  Copyright (C) 2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.abstractmarkup;

import net.sf.okapi.common.BaseParameters;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.ISimplifierRulesParameters;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.core.simplifierrules.ParseException;
import net.sf.okapi.core.simplifierrules.SimplifierRules;
import net.sf.okapi.filters.abstractmarkup.config.TaggedFilterConfiguration;

/**
 * {@link IParameters} based facade around the YAML configuration format.
 * The parameters are read-only; calls to setXXX() are ignored.
 */
public class AbstractMarkupParameters extends BaseParameters implements ISimplifierRulesParameters {
	
	private TaggedFilterConfiguration taggedConfig;
	private String title = "Parameters Editor";

	public AbstractMarkupParameters () {
		reset();
	}

	@Override
	public void fromString (String data) {
		taggedConfig = new TaggedFilterConfiguration(data);
	}

	@Override
	public String toString () {
		return taggedConfig.toString();
	}

	@Override
	public void reset () {		
		taggedConfig = new TaggedFilterConfiguration("collapse_whitespace: false\nassumeWellformed: true");
	}

	/**
	 * Gets the title to use with the parameter editor.
	 * @return the title to use with the parameter editor.
	 */
	public String getEditorTitle () {
		return title;
	}
	
	/**
	 * Sets the title to use with the parameters editor.
	 * @param title the title to use with the parameters editor.
	 */
	public void setEditorTitle (String title) {
		this.title = title;
	}

	/**
	 * Gets the TaggedFilterConfiguration object for this parameters object.
	 * @return the TaggedFilterConfiguration object for this parameters object.
	 */
	public TaggedFilterConfiguration getTaggedConfig() {
		return taggedConfig;
	}

	/**
	 * Sets the TaggedFilterConfiguration object for this parameters object.
	 * @param taggedConfig the TaggedFilterConfiguration object for this parameters object.
	 */
	public void setTaggedConfig(TaggedFilterConfiguration taggedConfig) {
		this.taggedConfig = taggedConfig;
	}

	@Override
	public boolean getBoolean(String name) {
		return taggedConfig.getBooleanParameter(name);
	}

	@Override
	public void setBoolean(String name, boolean value) {
	}

	@Override
	public String getString(String name) {
		return taggedConfig.getStringParameter(name);
	}

	@Override
	public void setString(String name, String value) {
	}

	@Override
	public int getInteger(String name) {
		return taggedConfig.getIntegerParameter(name);
	}

	@Override
	public void setInteger(String name, int value) {
	}
	
	@Override
	public String getSimplifierRules() {
		return taggedConfig.getSimplifierRules();
	}

	@Override
	public void setSimplifierRules(String rules) {
		taggedConfig.setSimplfierRules(rules);
	}

	@Override
	public void validateSimplifierRules() throws ParseException {
		SimplifierRules r = new SimplifierRules(getSimplifierRules(), new Code());
		r.parse();
	}
}
