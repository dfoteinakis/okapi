/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.xliff.its;

import java.util.Collection;

/**
 * Methods needed to properly implement the backend for storing ITS standoff
 * references.
 */
public interface IITSDataStore {

	/**
	 * Setup IITSDataStore with an identifier marking each time a new file
	 * is opened for parsing.
	 */
	public void initialize(String identifier);

	/**
	 * Return a list of stored LQI metadata URIs.
	 */
	public Collection<String> getStoredLQIURIs();

	/**
	 * Return a list of stored Provenance metadata URIs.
	 */
	public Collection<String> getStoredProvURIs();

	/**
	 * Fetch LQI metadata by URI.
	 */
	public ITSLQICollection getLQIByURI(String uri);

	/**
	 * Fetch Provenance metadata by URI.
	 */
	public ITSProvenanceCollection getProvByURI(String uri);

	/**
	 * Store LQI metadata.
	 */
	public void save(ITSLQICollection lqi);

	/**
	 * Store Provenance metadata.
	 */
	public void save(ITSProvenanceCollection prov);
}
