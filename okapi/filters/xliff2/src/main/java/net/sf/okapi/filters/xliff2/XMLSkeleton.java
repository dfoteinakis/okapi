/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
============================================================================*/

package net.sf.okapi.filters.xliff2;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.ISkeleton;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.INameable;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextFragment;

public class XMLSkeleton implements ISkeleton {

	private IResource parent;
	private StringBuilder before;
	private String snippet;
	
	@Override
	public String toString () {
		return (before==null ? "" : before.toString())
			+ getSnippetString();
	}
	
	@Override
	public ISkeleton clone () {
		XMLSkeleton newSkel = new XMLSkeleton();
		newSkel.parent = this.parent;
		newSkel.before = new StringBuilder(this.before);
		newSkel.snippet = this.snippet;
		return newSkel;
	}
	
	@Override
	public void setParent (IResource parent) {
		this.parent = parent;
	}

	@Override
	public IResource getParent () {
		return parent;
	}

	public void add (String data) {
		if ( data != null ) {
			if ( before == null ) before = new StringBuilder();
			before.append(data);
		}
	}

	public void addValuePlaceholder (INameable referent,
		String propName,
		LocaleId locId)
	{
		add(TextFragment.makeRefMarker("$self$", propName));
	}
	
	/**
	 * Adds to this skeleton a placeholder for the content (in a given locale) of the resource
	 * to which this skeleton is attached.
	 * @param textUnit the resource object.
	 * @param locId the locale; use null if the reference is the source.
	 */
	public void addContentPlaceholder (ITextUnit textUnit,
		LocaleId locId)
	{
		add(TextFragment.makeRefMarker("$self$"));
	}
	
	private String getSnippetString () {
		if (snippet == null) return "";
		return snippet;
	}
}
