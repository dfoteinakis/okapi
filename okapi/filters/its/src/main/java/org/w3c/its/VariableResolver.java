/*===========================================================================
  Copyright (C) 2012-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package org.w3c.its;

import java.util.Comparator;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPathVariableResolver;

/**
 * Resolver for XPath string variables.
 */
class VariableResolver implements XPathVariableResolver {
	
	class QNameReverseComparator implements Comparator<QName> {

		@Override
		public int compare (QName arg0,
			QName arg1)
		{
			return arg1.toString().compareTo(arg0.toString());
		}
		
	}

	private SortedMap<QName, String> table;
	
	/**
	 * Resolves the variable for a given name.
	 * @param qName the name of the variable.
	 * @return the value for the given name, or null if no value for that name exists.
	 */
	@Override
	public Object resolveVariable (QName qName) {
		if ( table == null ) return null;
		return table.get(qName);
	}

	/**
	 * Adds a variable and its value to this object. If the variable already exists, it is overwritten.
	 * @param qName the name of the variable.
	 * @param value the value.
	 * @param overwrite true to overwrite existing values, false to preserve old value.
	 */
	public void add (QName qName,
		String value,
		boolean overwrite)
	{
		if ( !overwrite && ( table != null )) {
			if ( table.containsKey(qName) ) return; // Do not overwrite
		}
		if ( table == null ) table = new TreeMap<QName, String>(new QNameReverseComparator());
		table.put(qName, value);
	}

	/**
	 * Replaces variables $qname by their value if they are in the resolver.
	 * <p>If a variable in the string is not declared in the resolver, it is not replaced.
	 * @param xpath the string where to replace the parameters (usually an XPath expression)
	 * @return the resulting string
	 */
	public String replaceVariables (String xpath) {
		if ( table == null ) return xpath;
		for ( QName qname : table.keySet() ) {
			xpath = xpath.replace("$"+qname.toString(), quote(table.get(qname)));
		}
		return xpath;
	}
	
	/**
	 * Provide a quoted version of the text passed as parameter.
	 * @param text the text to quote.
	 * @return the text with the proper quotes.
	 */
	private String quote (String text) {
		if ( text.indexOf('\'') != -1 ) {
			if ( text.indexOf('"') != -1 ) {
				//TODO: How to represent both type of quotes
				return "'"+text+"'";
			}
			return "\""+text+"\"";
		}
		return "'"+text+"'";
	}

}
