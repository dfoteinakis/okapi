/*===========================================================================
  Copyright (C) 2015 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.wordcount.common;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.steps.tokenization.common.Token;
import net.sf.okapi.steps.tokenization.tokens.Tokens;
import net.sf.okapi.steps.wordcount.CharacterCounter;

public abstract class TokenCharacterCountStep extends TokenCountStep {

	@Override
	protected long count(Segment segment, LocaleId locale) {
		return countTokenChars(getTokens(segment, locale), locale);
	}
	
	@Override
	protected long count(TextContainer textContainer, LocaleId locale) {
		return countTokenChars(getTokens(textContainer, locale), locale);
	}
	
	/**
	 * Return the total character count (calculated per {@link GMX#TotalCharacterCount})
	 * of all supplied tokens.
	 */
	public static long countTokenChars(Tokens tokens, LocaleId locale) {
		if (tokens == null || locale == null) {
			return 0L;
		}
		long total = 0L;
		for (Token t : tokens) {
			total += CharacterCounter.count(t.getValue(), locale);
		}
		return total;
	}
}
