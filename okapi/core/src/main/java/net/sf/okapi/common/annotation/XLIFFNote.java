/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.common.annotation;

/**
 * Annotation representing the XLIFF 1.2 note element.
 * The set of note elements should be contained within the XLIFFNoteAnnotation
 */
public class XLIFFNote {
	// Required
	private String note;
	// optional
	private String xmlLang;
	private String from;
	private String priority;
	private String annotates;
	
	public XLIFFNote() {	
	}
	
	public XLIFFNote(String note) {
		this.setNoteText(note);
	}

	public String getNoteText() {
		return note;
	}

	public void setNoteText(String note) {
		this.note = note;
	}

	public String getXmLang() {
		return xmlLang;
	}

	public void setXmlLang(String xmlLang) {
		this.xmlLang = xmlLang;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getAnnotates() {
		return annotates;
	}

	public void setAnnotates(String annotates) {
		this.annotates = annotates;
	}
}
