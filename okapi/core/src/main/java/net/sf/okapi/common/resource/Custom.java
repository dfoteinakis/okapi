package net.sf.okapi.common.resource;

import java.util.Collections;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.ISkeleton;
import net.sf.okapi.common.annotation.Annotations;
import net.sf.okapi.common.annotation.IAnnotation;
import net.sf.okapi.common.exceptions.OkapiNotImplementedException;

public class Custom implements IResource, IWithAnnotations {
	private Annotations annotations;
	private String id;

	@Override
	public <A extends IAnnotation> A getAnnotation(Class<A> annotationType) {
		if (annotations == null)
			return null;
		return annotationType.cast(annotations.get(annotationType));
	}

	@Override
	public String getId() {
		return id;
	}

	/**
	 * Always throws an exception as there is never a skeleton associated to a RawDocument.
	 * 
	 * @return never returns.
	 * @throws OkapiNotImplementedException this method is not implemented.
	 */
	@Override
	public ISkeleton getSkeleton() {
		throw new OkapiNotImplementedException("Custom does not have a skeketon");
	}

	@Override
	public void setAnnotation(IAnnotation annotation) {
		if (annotations == null) {
			annotations = new Annotations();
		}
		annotations.set(annotation);
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * This method has no effect as there is never a skeleton for a Document.
	 * 
	 * @param skeleton the skeleton.
	 * @throws OkapiNotImplementedException this method is not implemented.
	 */
	@Override
	public void setSkeleton(ISkeleton skeleton) {
		throw new OkapiNotImplementedException("Custom does not have a skeketon");
	}

	public Iterable<IAnnotation> getAnnotations () {
		if ( annotations == null ) {
			return Collections.emptyList();
		}
		return annotations;
	}
}
