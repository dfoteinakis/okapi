package net.sf.okapi.common.resource;

import java.util.Set;

public interface IWithProperties {

	/**
	 * Gets the resource-level property for a given name.
	 * @param name Name of the property to retrieve.
	 * @return The property or null if it does not exist.
	 */
	public Property getProperty (String name);

	/**
	 * Gets the names of all the resource-level properties for this resource.
	 * @return All the names of the resource-level properties for this resource.
	 */
	public Set<String> getPropertyNames ();

	/**
	 * Indicates if a resource-level property exists for a given name.
	 * @param name The name of the resource-level property to query.
	 * @return True if a resource-level property exists, false otherwise.
	 */
	public boolean hasProperty (String name);

	/**
	 * Removes a resource-level property of a given name. If the property does not exists
	 * nothing happens.
	 * @param name The name of the property to remove.
	 */
	public void removeProperty (String name);

	/**
	 * Sets a resource-level property. If a property already exists it is overwritten.
	 * @param property The new property to set.
	 * @return The property that has been set.
	 */
	public Property setProperty (Property property);

}
