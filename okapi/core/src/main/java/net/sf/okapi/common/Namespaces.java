/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
============================================================================*/

package net.sf.okapi.common;

/**
 * Defines commonly used namespaces and default prefixes.
 */
public final class Namespaces {

	public static final String XML_NS_URI       = "http://www.w3.org/XML/1998/namespace";
	public static final String XML_NS_PREFIX    = "xml";
	
	public static final String ITS_NS_URI       = "http://www.w3.org/2005/11/its";
	public static final String ITS_NS_PREFIX    = "its";
	
	public static final String ITSXLF_NS_URI    = "http://www.w3.org/ns/its-xliff/";
	public static final String ITSXLF_NS_PREFIX = "itsxlf";

	public static final String ITSX_NS_URI      = "http://www.w3.org/2008/12/its-extensions";
	public static final String ITSX_NS_PREFIX   = "itsx";
	
	public static final String XLINK_NS_URI     = "http://www.w3.org/1999/xlink";
	public static final String XLINK_NS_PREFIX  = "xlink";
	
	public static final String HTML_NS_URI      = "http://www.w3.org/1999/xhtml";
	public static final String HTML_NS_PREFIX   = "h";

	/**
	 * URI for the XLIFF 1.2 namespace.
	 */
	public static final String NS_XLIFF12       = "urn:oasis:names:tc:xliff:document:1.2";
	
	/**
	 * URI for the Okapi XLIFF extensions namespace.
	 */
	public static final String NS_XLIFFOKAPI    = "okapi-framework:xliff-extensions";
	
}
