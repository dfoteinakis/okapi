/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.languagetool.rules;

import java.io.IOException;

import org.languagetool.AnalyzedSentence;
import org.languagetool.rules.RuleMatch;
import org.languagetool.rules.bitext.BitextRule;
import org.languagetool.rules.patterns.PatternRule;

/**
 * A Term Check pattern rule class. A TermCheckRule describes a language error
 * where the source term is found, but not the target term.
 * 
 * @author jimh
 */
public class TermCheckRule extends BitextRule {

	private final PatternRule srcRule;
	private final PatternRule trgRule;

	public TermCheckRule(final PatternRule src, final PatternRule trg) {
		srcRule = src;
		trgRule = trg;
	}

	public PatternRule getSrcRule() {
		return srcRule;
	}

	public PatternRule getTrgRule() {
		return trgRule;
	}

	@Override
	public String getDescription() {
		return srcRule.getDescription();
	}

	@Override
	public String getMessage() {
		return trgRule.getMessage();
	}

	@Override
	public String getId() {
		return srcRule.getId();
	}

	/**
	 * This method always returns an empty array. Use
	 * {@link #match(org.languagetool.AnalyzedSentence, org.languagetool.AnalyzedSentence)}
	 * instead.
	 */
	@Override
	public RuleMatch[] match(AnalyzedSentence sentence) throws IOException {
		return new RuleMatch[0];
	}

	@Override
	public RuleMatch[] match(AnalyzedSentence sourceSentence, AnalyzedSentence targetSentence) throws IOException {
		RuleMatch[] matches = srcRule.match(sourceSentence);
		if (matches.length > 0) {
			// if we can't find the specified target term we have an error
			if (trgRule.match(targetSentence).length <= 0) {
				return matches;
			}
		}

		// otherwise we found the source and target terms
		return new RuleMatch[0];
	}

	@Override
	public void reset() {
	}

	@Override
	public String toString() {
		return getSrcRule().toPatternString();
	}
}
